define(['TestUtils'], function(TestUtils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    var utils = new TestUtils(),
        beforeEachCalled = false,
        afterEachCalled = false;

    describe('mocks', function() {

        describe('useMocks', function() {

            beforeEach(module('ng', function() {

                utils.mocks.useMocks([
                    {
                        type: 'service',
                        name: 'TestService',
                        value: function() {
                            function Service() {
                                this.methodA = function() {
                                    return 'A';
                                };
                            }
                            return new Service();
                        },
                        beforeEach: function() {
                            beforeEachCalled = true;
                        }
                    },
                    {
                        type: 'service',
                        name: 'TargetService',
                        value: function() {
                            function Service() {
                                this.methodA = function() {
                                    return 'A';
                                };
                            }
                            return new Service();
                        },
                        afterEach: function() {
                            afterEachCalled = true;
                        }
                    },
                    {
                        type: 'decorator',
                        name: 'TargetService',
                        value: function($delegate) {
                            return utils.spies.addSpies($delegate, 'returnValue', 'B');
                        }
                    },
                    {
                        name: 'TestValue',
                        value: 123
                    },
                    {
                        name: 'InvalidMock'
                    },
                    {
                        type: 'constant',
                        name: 'ConstantValue',
                        value: 'abc'
                    },
                    {
                        type: 'constant',
                        name: 'FalseValue',
                        value: false
                    },
                    {
                        type: 'factory',
                        name: 'TestFactory',
                        value: function() {
                            return {
                                methodA: function() {
                                    return 'A';
                                }
                            };
                        }
                    },
                    {
                        type: 'factory',
                        name: 'StatefulMock',
                        value: function() {
                            var subs = [];
                            return {
                                addSub: function(sub) {
                                    subs.push(sub);
                                },
                                getSubs: function() {
                                    return subs;
                                }
                            };
                        }
                    },
                    {
                        type: 'service',
                        name: 'InjectedService',
                        value: ['$timeout', function($timeout) {
                            return {
                                async: $timeout
                            };
                        }]
                    }
                ]);

            }));

            it('exists', function() {
                expect(utils.mocks.useMocks).toBeDefined();
                expect(typeof utils.mocks.useMocks).toBe('function');
            });

            it('does nothing if value not provided', function() {
                var ok;
                try {
                    // if value not registered,
                    // inject will throw error
                    inject(function(InvalidMock) {
                        // should not reach here
                        ok = false;
                    });
                } catch (e) {
                    ok = true;
                }
                expect(ok).toBe(true);
            });

            it('handles value', inject(function(TestValue) {
                expect(TestValue).toBe(123);
            }));

            it('handles false value', inject(function(FalseValue) {
                expect(FalseValue).toBe(false);
            }));

            it('handles constant', inject(function(ConstantValue) {
                expect(ConstantValue).toBe('abc');
            }));

            it('handles factory function', inject(function(TestFactory) {
                expect(TestFactory.methodA()).toBe('A');
            }));

            it('handles service function', inject(function(TestService) {
                expect(TestService.methodA()).toBe('A');
            }));

            it('handles decorator', inject(function(TargetService) {
                expect(TargetService.methodA()).toBe('B');
            }));

            it('invokes beforeEach if defined', function() {
                expect(beforeEachCalled).toBe(true);
            });

            it('invokes afterEach if defined', function() {
                expect(afterEachCalled).toBe(true);
            });

            it('allows for dependency injection', inject(function(InjectedService) {
                expect(InjectedService.async).toBeDefined();
            }));

            describe('mocks do not share state between tests', function() {

                it('', inject(function(StatefulMock) {
                    expect(StatefulMock.getSubs().length).toBe(0);
                    StatefulMock.addSub({});
                    expect(StatefulMock.getSubs().length).toBe(1);
                }));

                it('', inject(function(StatefulMock) {
                    expect(StatefulMock.getSubs().length).toBe(0);
                }));

            });

        });

    });

});
