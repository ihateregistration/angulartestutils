define(['TestUtils'], function(TestUtils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    var utils = new TestUtils();

    describe('spies', function() {

        describe('addSpies', function() {

            it('exists', function() {
                expect(utils.spies.addSpies).toBeDefined();
                expect(typeof utils.spies.addSpies).toBe('function');
            });

            it('spies on all methods if none specified - non-recursive', function() {
                var obj = {
                    nonMethod: 'abc',
                    methodA: function() {},
                    methodB: function() {},
                    inner: {
                        methodC: function() {}
                    }
                };
                utils.spies.addSpies(obj);
                expect(jasmine.isSpy(obj.methodA)).toBe(true);
                expect(jasmine.isSpy(obj.methodB)).toBe(true);
                expect(jasmine.isSpy(obj.inner.methodC)).toBe(false);
            });

            it('spies on all methods if none specified - recursive', function() {
                var obj = {
                    nonMethod: 'abc',
                    methodA: function() {},
                    methodB: function() {},
                    inner: {
                        methodC: function() {}
                    }
                };
                utils.spies.addSpies(obj, true);
                expect(jasmine.isSpy(obj.methodA)).toBe(true);
                expect(jasmine.isSpy(obj.methodB)).toBe(true);
                expect(jasmine.isSpy(obj.inner.methodC)).toBe(true);
            });

            it('adds spies for methods that were not on object', function() {
                var obj = {
                    methodA: function() {},
                    methodB: function() {}
                };
                utils.spies.addSpies(obj, ['methodC', 'methodD']);
                expect(jasmine.isSpy(obj.methodC)).toBe(true);
                expect(jasmine.isSpy(obj.methodD)).toBe(true);
            });

            it('does not add spies for unspecified methods', function() {
                var obj = {
                    methodA: function() {},
                    methodB: function() {}
                };
                utils.spies.addSpies(obj, ['methodC', 'methodD']);
                expect(jasmine.isSpy(obj.methodA)).toBe(false);
                expect(jasmine.isSpy(obj.methodB)).toBe(false);
            });

            it('returns obj passed in', function() {
                var obj = {my: 'obj'};
                expect(utils.spies.addSpies(obj)).toBe(obj);
            });

            it('uses specified action and args', function() {
                [
                    {action: 'callThrough', arg: null, output: 'abc'},
                    {action: 'callFake', arg: function() { return 'def'; }, output: 'def'},
                    {action: 'returnValue', arg: 123, output: 123}
                ].forEach(function(test) {
                    var obj = {
                        methodA: function() { return 'abc'; }
                    };
                    utils.spies.addSpies(obj, ['methodA'], test.action, test.arg);
                    expect(obj.methodA()).toBe(test.output);
                });
            });

            it('creates empty object if none passed in', function() {
                var mock = utils.spies.addSpies(['methodA', 'methodB']);
                expect(mock.methodA).toBeDefined();
                expect(mock.methodB).toBeDefined();
            });

        });

        describe('removeSpies', function() {

            it('exists', function() {
                expect(utils.spies.removeSpies).toBeDefined();
                expect(typeof utils.spies.removeSpies).toBe('function');
            });

            it('throws if no obj provided', function() {
                expect(utils.spies.removeSpies.bind(null)).toThrow();
                expect(utils.spies.removeSpies.bind(null, undefined, ['methodA'])).toThrow();
            });

            it('restores original functionality', function() {
                var obj = {
                    methodA: function() {
                        return 'A';
                    }
                };
                expect(obj.methodA()).toBe('A');
                utils.spies.addSpies(obj, 'returnValue', 'B');
                expect(obj.methodA()).toBe('B');
                utils.spies.removeSpies(obj);
                expect(obj.methodA()).toBe('A');
            });

            it('removes existing spies - non-recursive', function() {
                var obj = utils.spies.addSpies(['methodA', 'methodB']);
                expect(jasmine.isSpy(obj.methodA)).toBe(true);
                expect(jasmine.isSpy(obj.methodB)).toBe(true);
                utils.spies.removeSpies(obj);
                expect(jasmine.isSpy(obj.methodA)).toBe(false);
                expect(jasmine.isSpy(obj.methodB)).toBe(false);
            });

            it('removes existing spies - recursive', function() {
                var obj = {
                    inner: {
                        methodA: function() {
                            return 'A';
                        }
                    }
                };
                expect(obj.inner.methodA()).toBe('A');
                utils.spies.addSpies(obj, true, 'returnValue', 'B');
                expect(obj.inner.methodA()).toBe('B');
                utils.spies.removeSpies(obj, true);
                expect(obj.inner.methodA()).toBe('A');
            });

            it('does not modify un-spied methods', function() {
                var obj = {
                    methodA: function() {
                        return 'A';
                    },
                    methodB: function() {
                        return 'B';
                    }
                };
                utils.spies.addSpies(obj, ['methodB'], 'returnValue', 'C');
                expect(obj.methodA()).toBe('A');
                expect(obj.methodB()).toBe('C');
                utils.spies.removeSpies(obj);
                expect(obj.methodA()).toBe('A');
                expect(obj.methodB()).toBe('B');
            });

            it('deletes spies that were created', function() {
                var obj = utils.spies.addSpies({}, ['newMethod']);
                expect(obj.newMethod).toBeDefined();
                utils.spies.removeSpies(obj);
                expect(obj.newMethod).not.toBeDefined();
            });

            it('only removes specified spies', function() {
                var obj = utils.spies.addSpies({}, ['methodA', 'methodB']);
                expect(jasmine.isSpy(obj.methodA)).toBe(true);
                expect(jasmine.isSpy(obj.methodB)).toBe(true);
                utils.spies.removeSpies(obj, ['methodA']);
                expect(jasmine.isSpy(obj.methodA)).toBe(false);
                expect(jasmine.isSpy(obj.methodB)).toBe(true);
            });

            it('returns object passed in', function() {
                var obj = utils.spies.addSpies({}, ['A', 'B']);
                expect(utils.spies.removeSpies(obj)).toBe(obj);
            });

        });

    });

});
