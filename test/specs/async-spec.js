define(['TestUtils'], function(TestUtils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    var utils = new TestUtils();

    describe('async', function() {

        beforeEach(module('ng', 'ngMock'));

        describe('wait', function() {

            it('exists', function() {
                expect(utils.async.wait).toBeDefined();
                expect(typeof utils.async.wait).toBe('function');
            });

            it('returns promise', function() {
                expect(utils.async.wait().then).toBeDefined();
            });

            it('resolves after specified ms', function(done) {

                var ok = false,
                    setOK = function() {
                        ok = true;
                    };

                setTimeout(function() {
                    if (ok) {
                        throw new Error();
                    }
                }, 10);

                setTimeout(function() {
                    if (!ok) {
                        throw new Error();
                    }
                    done();
                }, 100);

                utils.async
                    .wait(50)
                    .then(setOK);

            });

            it('uses mock clock if installed', function(done) {
                utils.clock.install();
                utils.async
                    .wait(10)
                    .then(function() {
                        utils.clock.uninstall();
                        done();
                    });
            });

        });

        describe('waitUntil', function() {

            beforeEach(function() {
                this.isTrue = function() {
                    return true;
                };
            });

            it('is defined', function() {
                expect(utils.async.waitUntil).toBeDefined();
                expect(typeof utils.async.waitUntil).toBe('function');
            });

            it('returns promise', function() {
                expect(utils.async.waitUntil(this.isTrue).then).toBeDefined();
            });

            it('immediately resolves if predicate is true', function(done) {
                utils.async.waitUntil(this.isTrue).then(done);
            });

            it('resolves when predicate returns true', function(done) {
                var result = false;
                utils.async
                    .waitUntil(function() {
                        return result;
                    })
                    .then(done);
                setTimeout(function() {
                    result = true;
                }, 200);
            });

            it('resolves with predicate return value', function(done) {

                var res = null,
                    output = {key: 'value'},

                    objExists = function() {
                        return res;
                    },

                    setObj = function() {
                        res = output;
                    };

                setTimeout(setObj, 200);

                utils.async
                    .waitUntil(objExists)
                    .then(function(obj) {
                        expect(obj).toBe(output);
                        done();
                    });

            });

            it('resolves after multiple digest cycles', function(done) {

                var ok = false,
                    isOK = function() {
                        return ok;
                    };

                inject(function($q) {
                    $q  .when(true)
                        .then(function() {
                            $q  .when(true)
                                .then(function() {
                                    $q  .when(true)
                                        .then(function() {
                                            ok = true;
                                        });
                                });
                        });
                });

                utils.async
                    .waitUntil(isOK)
                    .then(done);

            });

        });

        describe('doCycle', function() {

            it('exists', function() {
                expect(utils.async.doCycle).toBeDefined();
                expect(typeof utils.async.doCycle).toBe('function');
            });

            it('accepts optional count', inject(function($rootScope) {
                spyOn($rootScope, '$apply').and.callThrough();
                expect($rootScope.$apply.calls.count()).toBe(0);
                utils.async.doCycle(4);
                expect($rootScope.$apply.calls.count()).toBe(4);
            }));

            it('accepts optional ms', inject(function($timeout) {
                spyOn($timeout, 'flush').and.callThrough();
                utils.async.doCycle(1, 25);
                expect($timeout.flush).toHaveBeenCalledWith(25);
            }));

            it('uses mock clock if installed', function() {
                utils.clock.install();
                spyOn(utils.clock, 'tick').and.callThrough();
                utils.async.doCycle(1, 25);
                expect(utils.clock.tick).toHaveBeenCalledWith(25);
                utils.clock.uninstall();
            });

            it('calls $rootScope.$apply', inject(function($rootScope) {
                spyOn($rootScope, '$apply').and.callThrough();
                expect($rootScope.$apply.calls.count()).toBe(0);
                utils.async.doCycle();
                expect($rootScope.$apply.calls.count()).toBe(1);
            }));

            it('resolves promises', function(done) {
                inject(function($q) {
                    var defer = $q.defer();
                    defer.promise.then(done);
                    defer.resolve();
                });
                utils.async.doCycle();
            });

        });

        describe('getSyncDefer', function() {

            it('exists', function() {
                expect(utils.async.getSyncDefer).toBeDefined();
                expect(typeof utils.async.getSyncDefer).toBe('function');
            });

            describe('resolve', function() {

                beforeEach(function() {
                    this.defer = utils.async.getSyncDefer();
                });

                it('exists', function() {
                    expect(this.defer.resolve).toBeDefined();
                    expect(typeof this.defer.resolve).toBe('function');
                });

                it('notifies then callbacks', function(done) {
                    this.defer.promise.then(done);
                    this.defer.resolve();
                });

                it('passes resolved value to callbacks', function(done) {
                    this.defer.promise.then(function(val) {
                        expect(val).toBe('123');
                        done();
                    });
                    this.defer.resolve('123');
                });

                it('notifies finally callbacks', function(done) {
                    this.defer.promise.finally(done);
                    this.defer.resolve();
                });

            });

            describe('reject', function() {

                beforeEach(function() {
                    this.defer = utils.async.getSyncDefer();
                });

                it('exists', function() {
                    expect(this.defer.reject).toBeDefined();
                    expect(typeof this.defer.reject).toBe('function');
                });

                it('notifies catch callbacks', function(done) {
                    this.defer.promise.catch(done);
                    this.defer.reject();
                });

                it('passes rejection reason to callbacks', function(done) {
                    this.defer.promise.catch(function(reason) {
                        expect(reason).toBe('123');
                        done();
                    });
                    this.defer.reject('123');
                });

                it('notifies finally callbacks', function(done) {
                    this.defer.promise.finally(done);
                    this.defer.reject();
                });

            });

            describe('promise', function() {

                beforeEach(function() {
                    this.defer = utils.async.getSyncDefer();
                });

                it('exists', function() {
                    expect(this.defer.promise).toBeDefined();
                });

                describe('then', function() {

                    it('ignores non-function arguments', function(done) {
                        this.defer.promise.then('123');
                        this.defer.promise.then(done);
                        this.defer.resolve();
                    });

                    it('registers single then callback', function(done) {
                        this.defer.promise.then(done);
                        this.defer.resolve();
                    });

                    it('registers then and catch callbacks', function(done) {
                        this.defer.promise.then(null, done);
                        this.defer.reject();
                    });

                    it('invokes callback if already resolved', function(done) {
                        this.defer.resolve();
                        this.defer.promise.then(done);
                    });

                    it('can be chained', function(done) {
                        var num = 0,
                            inc = function() {
                                num++;
                            };
                        this.defer.promise
                            .then(inc)
                            .then(inc)
                            .then(inc)
                            .then(function() {
                                expect(num).toBe(3);
                            })
                            .then(done);
                        this.defer.resolve();
                    });

                });

                describe('catch', function() {

                    it('ignores non-function arguments', function(done) {
                        this.defer.promise.catch('123');
                        this.defer.promise.catch(done);
                        this.defer.reject();
                    });

                    it('registers single callback', function(done) {
                        this.defer.promise.catch(done);
                        this.defer.reject();
                    });

                    it('invokes callback if already rejected', function(done) {
                        this.defer.reject();
                        this.defer.promise.catch(done);
                    });

                    it('can be chained', function(done) {
                        var num = 0,
                            inc = function() {
                                num++;
                            };
                        this.defer.promise
                            .catch(inc)
                            .catch(inc)
                            .catch(inc)
                            .catch(function() {
                                expect(num).toBe(3);
                            })
                            .catch(done);
                        this.defer.reject();
                    });

                });

                describe('finally', function() {

                    it('ignores non-function argument', function(done) {
                        this.defer.promise.finally(null);
                        this.defer.promise.finally(done);
                        this.defer.resolve();
                    });

                    it('registers single callback', function(done) {
                        this.defer.promise.finally(done);
                        this.defer.resolve();
                    });

                    it('invokes callback if already resolved', function(done) {
                        this.defer.resolve();
                        this.defer.promise.finally(done);
                    });

                    it('invokes callback if already rejected', function(done) {
                        this.defer.reject();
                        this.defer.promise.finally(done);
                    });

                    it('can be chained', function(done) {
                        var num = 0,
                            inc = function() {
                                num++;
                            };
                        this.defer.promise
                            .finally(inc)
                            .finally(inc)
                            .finally(inc)
                            .finally(function() {
                                expect(num).toBe(3);
                            })
                            .finally(done);
                        this.defer.resolve();
                    });

                });

            });

        });

    });

});
