define(['TestUtils'], function(TestUtils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    var utils = new TestUtils();

    describe('utils', function() {

        describe('attach', function() {

            it('exists', function() {
                expect(utils.attach).toBeDefined();
                expect(typeof utils.attach).toBe('function');
            });

            it('does not overwrite existing method', function(done) {
                var obj = {
                    method: done
                };
                utils.attach('method', function() { throw new Error(); }, obj);
                obj.method();
            });

            it('binds new method to specified context', function(done) {
                var obj = {};
                utils.attach('method', done, obj);
                obj.method();
            });

            it('uses `utils` as context if none specified', function(done) {
                utils.attach('method', done);
                utils.method();
            });

            it('can handle namespaced methods', function(done) {
                utils.attach('nested.ns.method', done);
                utils.nested.ns.method();
            });

        });

        describe('attempt', function() {

            it('respects pre-bound functions (1)', function() {
                var context = {
                    value: 1,
                    method: function() {
                        return context.value;
                    }
                };
                expect(utils.attempt(context.method)).toBe(context.value);
            });

            it('respects pre-bound functions (2)', function() {
                var context = {value: 1},
                    method = function() {
                        return this.value;
                    }.bind(context);
                expect(utils.attempt(method)).toBe(context.value);
            });

            it('passes subsequent args to function', function() {
                function test(a, b) {
                    expect(a).toBe('a');
                    expect(b).toBe('b');
                    return 'ok';
                }
                expect(utils.attempt(test, 'a', 'b')).toBe('ok');
            });

            it('returns true if function does not return anything', function() {
                expect(utils.attempt(function nop() {})).toBe(true);
            });

            it('returns whatever function returns', function() {
                ['abc', 123, new Date(), {key: 'value'}, null, false].forEach(
                    function(output) {
                        expect(utils.attempt(function() {
                            return output;
                        })).toBe(output);
                    });
            });

            it('returns false if function throws an exception', function() {
                expect(utils.attempt(function() {
                    throw new Error();
                })).toBe(false);
            });

        });

        describe('first', function() {

            it('accepts arguments', function() {
                expect(utils.first('abc', 'def', 'ghi')).toBe('abc');
            });

            it('returns undefined for no arguments', function() {
                expect(utils.first()).toBeUndefined();
            });

            it('uses function return values', function() {
                expect(utils.first(
                    function() { return null; },
                    function() { throw new Error(); },
                    function() { return 'abc'; }
                )).toBe('abc');
            });

            it('uses last value when no other truthy results', function() {
                expect(utils.first(0, '', NaN, false, 'abc')).toBe('abc');
            });

        });

    });

});
