define(['TestUtils'], function(TestUtils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    var utils = new TestUtils();

    describe('data', function() {

        describe('require', function() {

            it('is defined', function() {
                expect(utils.data.require).toBeDefined();
                expect(typeof utils.data.require).toBe('function');
            });

            it('returns promise', function() {
                var keys = Object.keys(utils.data.require()),
                    expected = ['then', 'finally', 'catch'];
                expected.every(function(key) {
                    expect(keys).toContain(key);
                });
            });

            it('resolves with valid path contents', function(done) {
                utils
                    .data.require('TestUtils')
                    .then(function(IncomingTestUtils) {
                        expect(utils instanceof IncomingTestUtils).toBe(true);
                        done();
                    });
            });

            it('rejects with invalid path', function(done) {
                utils
                    .data.require('InvalidPath')
                    .catch(done);
            });

            it('attaches success and failure handlers, if provided', function(done) {
                var successCalled = false,
                    failureCalled = false,
                    doSuccess = function() {
                        successCalled = true;
                    },
                    doFailure = function() {
                        failureCalled = true;
                    },
                    throwHere = function() {
                        throw new Error();
                    };
                utils
                    .data.require('TestUtils', doSuccess, throwHere)
                    .then(function() {
                        expect(successCalled).toBe(true);
                        utils
                            .data.require('InvalidPath', throwHere, doFailure)
                            .catch(function() {
                                expect(failureCalled).toBe(true);
                                done();
                            });
                    });
            });

        });

    });

});
