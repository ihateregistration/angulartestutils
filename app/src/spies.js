define(['utils'], function(utils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    /**
     * Adds spy functionality. Spies allow unit tests to
     * verify that methods have been called and also change
     * how an object's methods behave during testing.
     * @mixin spies
     */

    return function() {

        utils.apply(this);

        /**
         * Provides functionality to spy on an object's
         * existing methods or to create new spy methods.
         * @function spies#addSpies
         * @param {Object} [obj] The object to spy on. If not specified,
         *  a new empty object will be created: `{}`
         * @param {Array} [methods] The method names to spy on; if the
         *  method does not exist at the top-most level of the object,
         *  it will be created as a spy.
         * @param {Boolean} [recurse=false] `true` to spy on nested
         *  methods. By default, only  methods at the top-level of an
         *  object will be spied on.
         * @param {String} [action=callThrough] The spy action to attach
         *  to the specified methods. Possible actions include:
         *   - stub: the spy method should do nothing and return nothing
         *   - callFake: invoke another function in place of the real one
         *   - returnValue: always return the specified value
         *   - callThrough: invoke the underlying spied-upon function
         * @param {*} [actionArg] The optional argument to pass to the
         *  specified spy action. For example, if you specified `returnValue`
         *  as your spy action, `actionArg` would be the value you want
         *  your spy to return.
         * @returns {Object} The original object passed in, or a new
         *  object if no value was provided for `obj`.
         * @example
         * var obj = {
         *   prop: 'abc',
         *   methodA: function() {},
         *   methodB: function() {},
         *   inner: {
         *     methodC: function() {}
         *   }
         * };
         * utils.spies.addSpies(obj, ['methodA']); // spies only on methodA
         * utils.spies.addSpies(obj, ['methodD']); // creates new spy methodD
         * utils.spies.addSpies(obj, true); // adds spies on all un-spied methods,
         *                                  // including nested methods (methodC)
         * @example
         * // addSpies returns the object you pass in, so you can
         * // quickly create new mock objects or extend existing mocks:
         * utils.useMocks([
         *   {
         *     name: 'TargetService',
         *     value: utils.spies.addSpies(['methodA', 'methodB'])
         *   },
         *   {
         *     name: 'TargetModel',
         *     value: utils.spies.addSpies(someExistingObject, ['newMethod'])
         *   }
         * ]);
         * @example
         * utils.useMocks([
         *   name: 'SimpleService',
         *   value: utils.spies.addSpies(['methodA'], 'returnValue', 123)
         * ]);
         */
        this.attach('spies.addSpies', function addSpies(obj, methods, recurse, action, actionArg) {

            var args = this.misc.reorderByType(arguments, 'o?a?b?s?*?'),

                doSpy = function doSpy(target, method) {

                    var spy = target[method],
                        orig = target[method];

                    if (!jasmine.isSpy(spy)) {
                        target[method] = target[method] || function() {};
                        spy = spyOn(target, method);
                        spy.orig = orig;
                    }

                    if (action === 'remove') {
                        target[method] = spy.orig;
                        if (!spy.orig) {
                            delete target[method];
                        }
                    } else {
                        if (!!spy.and) { // Jasmine 2
                            spy.and[action](actionArg);
                        } else { // use direct access
                            spy[action](actionArg);
                        }
                    }

                },

                doSpyAll = function doSpyAll(target, recurse) {
                    if (Object.prototype.toString.call(target) === '[object Object]') {
                        Object
                            .keys(target)
                            .forEach(function(key) {
                                if (typeof target[key] === 'function') {
                                    doSpy(target, key);
                                } else if (recurse) {
                                    doSpyAll(target[key]);
                                }
                            });
                    }
                };

            obj = args[0] || {};
            methods = args[1] || [];
            recurse = args[2] || false;
            action = args[3] || 'callThrough';
            actionArg = args[4];

            if (methods.length === 0) {
                doSpyAll(obj, recurse);
            } else {
                methods.forEach(function(method) {
                    doSpy(obj, method);
                });
            }

            return obj;

        });

        /**
         * Removes any spies on the given object, restoring it to its original state.
         * @function spies#removeSpies
         * @param {Object} obj The object to remove spies from.
         * @param {Array} [methods] The spies to remove. If not specified, all
         *  spies will be removed.
         * @param {Boolean} [recurse=false] `true` to remove all nested spies; the
         *  default is to only remove spies at the top-level of an object.
         * @returns {Object} The original object passed in.
         * @throws Parameter `obj` is required.
         * @example
         * var utils = new TestUtils(),
         *     orig = { methodA: function() { return 1; } },
         *     mock = utils.spies.addSpies(orig, 'returnValue', 2);
         * mock.methodA(); // 2
         * utils.spies.removeSpies(mock);
         * mock.methodA(); // 1 (the original return value)
         * @example
         * var utils = new TestUtils(),
         *     mock = utils.spies.addSpies(['methodA', 'methodB'], 'stub');
         * mock.methodA(); // does nothing
         * mock.methodB(); // does nothing
         * utils.spies.removeSpies(mock, ['methodB']);
         * mock.methodA(); // does nothing
         * mock.methodB(); // error -- method does not exist
         */
        this.attach('spies.removeSpies', function removeSpies(obj, methods, recurse) {
            if (!obj) {
                throw new Error('Parameter `obj` is required.');
            }
            return this.spies.addSpies(obj, methods, recurse, 'remove');
        });

    };

});
