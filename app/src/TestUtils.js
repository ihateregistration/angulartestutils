define([
    'poly',
    'data',
    'async',
    'mocks',
    'utils',
    'spies'
], function(
    poly,
    data,
    async,
    mocks,
    utils,
    spies
) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    poly.apply();

    /**
     * Provides useful features for testing
     * AngularJS applications. The TestUtils
     * class takes a modular, mixin approach
     * that allows developers to add helpful
     * functionality at any time.
     * @class TestUtils
     * @mixes poly
     * @mixes utils
     * @mixes async
     * @mixes mocks
     * @mixes spies
     * @mixes data
     */
    return function TestUtils() {
        utils.apply(this);
        async.apply(this);
        mocks.apply(this);
        spies.apply(this);
        data.apply(this);
    };

});
