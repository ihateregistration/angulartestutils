define(['utils'], function(utils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    /**
     * Provides functionality related to mock objects,
     * which are stand-ins for real objects. Using mocks
     * during unit testing isolates your component,
     * ensuring your tests fail or succeed based solely
     * on the functionality being tested.
     * @mixin mocks
     */

    var appliedMocks = [],
        afterEachApplied = false;

    return function() {

        var self = this;

        utils.apply(this);

        /* jshint -W030 */

        afterEachApplied || afterEach(function tearDownMocks() {
            while (!!appliedMocks.length) {
                var mock = appliedMocks.pop();
                !!mock.afterEach && mock.afterEach();
            }
        });

        afterEachApplied = true;

        /**
         * Registers one or more mocks with Angular. You should first
         * load the Angular modules containing the components you wish
         * to mock.
         *
         * A mock object is a simple POJO that has at least 2
         * properties: `name` and `value`:
         *
         * - `name` is the name of the Angular component your mock
         * version should replace. When Angular attempts to inject
         * the original component, the mock version will be used instead.
         *
         * - `value` is the thing to pass to the Angular injector. Angular
         * expects different values for different types of mocks.
         *
         * - `type` is an optional property. If not specified, useMocks
         * will assume a simple value type, which can be used for values,
         * constants, services, and factories. The available types are:
         *
         *  - value
         *  - constant
         *  - service
         *  - factory
         *  - decorator
         * @function mocks#useMocks
         * @param {Array} mocks The list of mocks to register.
         * @example
         * var utils = new TestUtils();
         *
         * beforeEach(module('MyAngularModule', function() {
         *
         *   utils.mocks.useMocks([
         *     {
         *       // because type was not specified, we assume
         *       // a simple value type; this replaces the real
         *       // MyService component with a simple object
         *       // having 2 methods:
         *       name: 'MyService',
         *       value: {
         *         methodA: function() { ... },
         *         methodB: function() { ... }
         *       }
         *     },
         *     {
         *       // we can also decorate an existing service using
         *       // the `decorator` type:
         *       type: 'decorator',
         *       name: '$q',
         *       value: function($delegate) {
         *         $delegate.defer = utils.async.getSyncDefer;
         *         // now any calls to $q.defer will use the
         *         // getSyncDefer method and return a synchronous
         *         // promise that invokes callbacks immediately
         *         // upon resolution or rejection
         *         return $delegate;
         *       }
         *     },
         *     {
         *       // Angular expects `service` and `factory` types
         *       // to provide a function that will be invoked
         *       // to provide the resulting object:
         *       type: 'service',
         *       name: 'AnotherService',
         *       value: function() {
         *         function MyServiceType() {
         *           this.methodA = function() { ... };
         *           this.methodB = function() { ... };
         *         };
         *         return new MyServiceType();
         *       }
         *     },
         *     {
         *       // you can also use dependency injection:
         *       type: 'factory',
         *       name: 'InjectedService',
         *       value: ['$timeout', function($to) {
         *         return {
         *           doMethod: function() { ... },
         *           doMethodAsync: function() {
         *             return $to(this.doMethod);
         *           }
         *         };
         *       }];
         *     }
         *   ]);
         *
         * });
         */
        this.attach('mocks.useMocks', function useMocks(mocks) {
            module(function setUpMocks($provide) {
                mocks.forEach(function registerMock(mock) {
                    var provideMethod = $provide[mock.type || 'value'];
                    if (mock.value !== undefined && self.attempt(provideMethod, mock.name, mock.value)) {
                        appliedMocks.push(mock);
                        !!mock.beforeEach && mock.beforeEach();
                    } else {
                        console.error('could not register mock:', mock.name);
                    }
                });
            });
        });

    };

});
