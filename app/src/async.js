define(['angular', 'utils'], function(angular, utils) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    /**
     * Contains features useful for testing
     * asynchronous code, such as Angular promises
     * and window timeouts/intervals.
     * @mixin async
     */

    return function() {

        utils.apply(this);

        /**
         * The default mock clock used to control
         * intervals and timeouts in the application. To
         * enable the mock clock, call install().
         *
         * **IMPORTANT:** Make sure you call uninstall()
         * when finished or you could cause unwanted
         * side-effects in other unit tests.
         *
         * **NOTE:** You can overwrite the mock clock
         * with one of your own; just make sure your
         * mock clock has a `tick` method that accepts
         * the number of milliseconds to advance; there
         * is code throughout TestUtils that uses it.
         * @member {jasmine.Clock} async#clock
         * @example
         * var utils = new TestUtils();
         *
         * describe('mock clock', function() {
         *
         *   beforeEach(function() {
         *     utils.clock.install();
         *   });
         *
         *   it('example', function(done) {
         *     window.setTimeout(function() {
         *       // this will not be called
         *       // until the mock clock is
         *       // advanced at least 20ms
         *       done();
         *     }, 20);
         *     // advance the clock 20ms;
         *     // this will fire the timeout
         *     // callback registered above
         *     utils.clock.tick(20);
         *   });
         *
         *   afterEach(function() {
         *     utils.clock.uninstall();
         *   });
         *
         * });
         */
        this.clock = this.first(
            function() { return jasmine.clock(); },
            function() { return jasmine.Clock; },
            {
                tick: function() {},
                install: function() {},
                uninstall: function() {}
            }
        );

        /**
         * Returns a new defer-like object that resolves
         * or rejects synchronously -- useful for replacing
         * asynchronous promises like $q.
         *
         * **NOTE:** This is a simple promise-like object; it
         * has the following limitations:
         *
         *  - thrown exceptions will *not* reject the promise
         *  - if a resolver returns a value, it will be passed to
         *    all additional resolvers, regardless of where they
         *    were attached to the promise chain -- there is no
         *    concept of a parent/child relationship
         *
         * See the examples for details.
         * @function async#getSyncDefer
         * @returns {Object} A defer-like object that has
         * the following properties and methods:
         *
         *  - promise: A promise-like object with handlers
         *    for then, catch, and finally.
         *  - resolve: Method to resolve the promise; accepts
         *    an optional value that will be passed to any
         *    callbacks registered by calling promise.then()
         *  - reject: Method to reject the promise; accepts
         *    an optional reason that will be passed to any
         *    callbacks registered by calling promise.catch()
         * @example
         * var utils = new TestUtils();
         *
         * function myAsyncFunc() {
         *     var defer = utils.async.getSyncDefer();
         *     window.setTimeout(defer.resolve, 100);
         *     return defer.promise;
         * }
         * @example
         * // decorate an existing service to return a
         * // synchronous promise instead of its usual
         * // asynchronous version:
         * var utils = new TestUtils();
         * utils.mocks.useMocks([
         *   {
         *     type: 'decorator',
         *     name: 'SomeService',
         *     value: function($delegate) {
         *       var defer = utils.async.getSyncDefer();
         *       spyOn($delegate, 'someAsyncMethod').and
         *         .returnValue(defer.promise);
         *       $delegate.doReject = defer.reject;
         *       $delegate.doApprove = defer.resolve;
         *       return $delegate;
         *     }
         *   }
         * ]);
         * @example
         * // replace all asynchronous promises with
         * // a synchronous version:
         * var utils = new TestUtils();
         * utils.mocks.useMocks([
         *   {
         *     type: 'decorator',
         *     name: '$q',
         *     value: function($delegate) {
         *       $delegate.defer = utils.async.getSyncDefer;
         *       // now any calls to $q.defer will use the
         *       // getSyncDefer method and return a synchronous
         *       // promise that invokes callbacks immediately
         *       // upon resolution or rejection
         *       return $delegate;
         *     }
         *   }
         * ]);
         * @example
         * var defer = async.getSyncDefer();
         *
         * defer.promise
         *   .then(function(origVal) {
         *     // this gets the original resolved
         *     // value but changes it for subsequent
         *     // promises:
         *     return newVal;
         *   })
         *   .then(function(newVal) {
         *     // this gets the modified value
         *     // from the previous handler
         *   });
         *
         * defer.promise.then(function(newVal) {
         *   // this also gets newVal, even
         *   // though it was attached to the
         *   // original promise
         * });
         */
        this.attach('async.getSyncDefer', function getSyncDefer() {

            var fn1 = [],
                fn2 = [],
                fn3 = [],

                state = '',
                arg = null,

                invoke = function invoke(arr, val, overwrite) {
                    var res;
                    arr.forEach(function doCallback(fn) {
                        res = fn(val);
                        if (!!overwrite && res !== undefined) {
                            val = res;
                        }
                    });
                    return res;
                };

            /**
             * 'then', 'catch', and 'finally' are reserved
             * keywords in JavaScript; we need to tell JSCS
             * to ignore that particular rule for now so we
             * can use them as properties on our sync promise
             */

            // jscs:disable disallowQuotedKeysInObjects

            return {

                promise: {

                    'then': function(fn, cfn) {
                        if (typeof fn === 'function') {
                            if (state === 'resolved') {
                                fn(arg);
                            } else {
                                fn1.push(fn);
                            }
                        }
                        return this.catch(cfn);
                    },

                    'catch': function(fn) {
                        if (typeof fn === 'function') {
                            if (state === 'rejected') {
                                fn(arg);
                            } else {
                                fn3.push(fn);
                            }
                        }
                        return this;
                    },

                    'finally': function(fn) {
                        if (typeof fn === 'function') {
                            if (state !== '') {
                                fn(arg);
                            } else {
                                fn2.push(fn);
                            }
                        }
                        return this;
                    }

                },

                resolve: function(val) {
                    if (state === '') {
                        state = 'resolved';
                        arg = invoke(fn1, val, true);
                        invoke(fn2, arg);
                    }
                },

                reject: function(reason) {
                    if (state === '') {
                        state = 'rejected';
                        arg = invoke(fn3, reason, true);
                        invoke(fn2, arg);
                    }
                }

            };

        });

        /**
         * Progresses the application forward by 10ms and a single
         * Angular digest cycle. You can also specify how many cycles
         * and how many milliseconds should advance.
         * @function async#doCycle
         * @param {Number} [num=1] The number of cycles to invoke.
         * @param {Number} [ms=10] The number of milliseconds to
         *  advance the mock clock (or $interval and $timeout, if
         *  the mock clock has not been installed).
         * @example
         * var utils = new TestUtils();
         *
         * it('some async test', function(done) {
         *   inject(function($q) {
         *     var defer = $q.defer();
         *     defer.promise.then(done);
         *     defer.resolve();
         *   });
         *   // Angular resolves promises on the
         *   // next digest cycle, but unit tests
         *   // run within a single digest cycle,
         *   // so `done` will never be invoked;
         *   // we have to force the next digest
         *   // cycle so Angular will invoke any
         *   // callbacks passed to then(...):
         *   utils.async.doCycle();
         * });
         */
        this.attach('async.doCycle', function doCycle(num, ms) {

            ms = ms || 10;
            num = num || 1;

            // can't use .bind because inject()
            // needs its original context to find
            // and inject the correct objects
            var self = this;

            inject(function($rootScope, $timeout, $interval) {

                var i = 0,
                    attempt = self.attempt,
                    tick = self.clock.tick,
                    flush1 = $timeout.flush,
                    flush2 = $interval.flush,
                    apply = $rootScope.$apply.bind($rootScope);

                for (; i < num; i++) {
                    if (!attempt(tick, ms)) {
                        attempt(flush1, ms);
                        attempt(flush2, ms);
                    }
                    attempt(apply);
                }

            });

        });

        /**
         * Waits the specified number of milliseconds before resolving
         * the returned promise. Useful for ordering execution during
         * tests.
         * @function async#wait
         * @param {Number} [ms=10] The number of milliseconds to wait
         *  before resolving the returned promise;
         * @returns {Object} A synchronous promise. See
         *  [getSyncDefer](#getSyncDefer) for details.
         * @example
         * var utils = new TestUtils();
         * it('ordered test', function(done) {
         *   inject(function(MyService) {
         *     utils.async.wait(100).then(MyService.methodA);
         *     utils.async.wait(200).then(MyService.methodB);
         *     utils.async.wait(300).then(done);
         *   });
         * });
         */
        this.attach('async.wait', function wait(ms) {

            ms = ms || 10;

            var defer = this.async.getSyncDefer();
            setTimeout(defer.resolve, ms);
            this.attempt(this.clock.tick, ms);

            return defer.promise;

        });

        /**
         * Invokes [doCycle](#doCycle) until the predicate function
         *  returns a truthy value, then resolves the returned promise
         *  with that value.
         * @function async#waitUntil
         * @param {Function} predicate A function to invoke. If the
         *  result is truthy, the returned promise will be resolved.
         * @returns {Object} A synchronous promise. See
         *  [getSyncDefer](#getSyncDefer) for details.
         * @example
         * var utils = new TestUtils();
         *
         * // inject a service and defer test
         * // execution until it has completely
         * // initialized (e.g., it may need to
         * // perform some async configuration)
         *
         * beforeEach(function(done) {
         *   var self = this;
         *   inject(function(SomeService) {
         *     self.service = SomeService;
         *     utils.async
         *       .waitUntil(self.service.isReady)
         *       .then(done);
         *   });
         * });
         *
         * it('this.service is ready...', function() { ... });
         */
        this.attach('async.waitUntil', function waitUntil(predicate) {

            var val,
                defer = this.async.getSyncDefer(),
                doCycle = this.async.doCycle,
                loop = setInterval(function doLoop() {
                    if (!!(val = predicate())) {
                        clearInterval(loop);
                        defer.resolve(val);
                    }
                    doCycle();
                }, 10);

            this.attempt(this.clock.tick, 10);

            return defer.promise;

        });

    };

});
