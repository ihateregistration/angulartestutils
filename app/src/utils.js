define(function() {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    /**
     * Provides functionality used by other mixins that may also
     * be useful to test developers.
     * @mixin utils
     */

    var validators = {
            n: function(val) { return typeof val === 'number'; },
            s: function(val) { return typeof val === 'string'; },
            b: function(val) { return typeof val === 'boolean'; },
            f: function(val) { return typeof val === 'function'; },
            O: function(val) { return typeof val === 'object'; },
            a: function(val) { return val instanceof Array; },
            d: function(val) { return val instanceof Date; },
            r: function(val) { return val instanceof RegExp; },
            o: function(val) { return typeof val === 'object' && !(val instanceof Array) && !(val instanceof Date) && !(val instanceof RegExp); },
            '*': function(val) { return val !== undefined; }
        },

        is = function(val, sym) {
            return validators[sym](val);
        },

        verifyTypePattern = function(pattern) {

            var wasOptional = false,
                last = {},
                allowedSyms = Object.keys(validators),
                objectTypes = 'adro',
                i = 0, l = pattern.length;

            for (; i < l; i++) {

                if (allowedSyms.indexOf(pattern[i]) === -1) {
                    throw new Error('Unexpected symbol "' + pattern[i] + '" in expand pattern.');
                }

                if (pattern[i + 1] === '?') {

                    wasOptional = true;
                    last[pattern[i]] = true;
                    i++;

                } else {

                    if (wasOptional && (last[pattern[i]] || (last.O && objectTypes.indexOf(pattern[i]) !== -1) || (pattern[i] === 'O' && (last.a || last.d || last.r || last.o)))) {
                        throw new Error('You can\'t use required argument after optional arguments sequence which include the same or similar type in expand pattern.');
                    }

                    wasOptional = false;
                    last = {};

                }

            }

        },

        reorderListByTypes = function(list, pattern) {

            verifyTypePattern(pattern);

            list = Array.prototype.slice.call(list);

            var args = [],
                i = 0,
                j = 0,
                l = pattern.length,
                numExpected = pattern.replace(/\?/g, '').length,
                lastFound = 0;

            for (; i < l; i++) {

                if (is(list[j], pattern[i])) {
                    lastFound = j;
                    args.push(list[j++]);
                    if (pattern[i + 1] === '?') {
                        i++;
                    }
                    continue;
                }

                if (pattern[i + 1] === '?') {
                    args.push(undefined);
                    i++;
                    continue;
                }

                throw new Error('Passed arguments list does not match expected pattern.');

            }

            if (j < list.length) {
                args.splice(j + 1);
                args = args.concat(list.slice(j + 1));
                while (args.length < numExpected) {
                    args.push(undefined);
                }
            }

            return args;

        };

    return function() {

        /**
         * Attaches a method to the given context. Used for mixins.
         * @function utils#attach
         * @param {String} name The name of the function to attach. The name
         *  can include any number of namespaces -- if they do not already
         *  exist, they will be created.
         * @param {Function} fn The function to attach to the specified context.
         * @param {Object} [context] The context in which to execute the function.
         *  If not provided, defaults to `this`.
         * @example
         * utils.apply(myObject); // adds 'attach' method to myObject
         * myObject.attach('my.ns.method', function(param) {...});
         * myObject.my.ns.method('param-value');
         * @example
         * utils.attach('ns.method', function() {...}, someObject);
         * someObject.ns.method();
         */
        this.attach = this.attach || function attach(name, fn, context) {

            context = context || this;

            var index = name.indexOf('.'),
                scope = context,
                ns = '';

            while (index !== -1) {
                ns = name.substring(0, index);
                scope[ns] = scope[ns] || {};
                scope = scope[ns];
                name = name.substring(index + 1);
                index = name.indexOf('.');
            }

            scope[name] = scope[name] || fn.bind(context);

        }.bind(this);

        /**
         * Tries to execute the specified function. If an exception
         * is thrown, will return false; otherwise, returns the
         * value of the executed function (or true, if the function
         * does not return any value, or returns undefined).
         *
         * **NOTE:** If the function requires access to `this`, you
         * should pre-bind it. See the examples for details.
         * @function utils#attempt
         * @param {Function} fn The function to invoke.
         * @param {*} [args] Any additional arguments you provide
         *  will be passed to the function as parameters.
         * @returns {*} `false` if the function throws an exception;
         *  otherwise, returns the result of the function, or `true`
         *  if the function has no return value.
         * @example
         * utils.attempt(myObj.methodA, 'arg1', 'arg2');
         * @example
         * if (var res = utils.attempt(myObj.methodB)) {
         *   console.log('methodB returned:', res);
         * }
         * @example
         * var obj = {
         *   prop: 'abc',
         *   getProp: function() {
         *     return this.prop;
         *   }
         * };
         * // because getProp accesses `this`, we need to pre-bind
         * // the function with the appropriate context reference:
         * utils.attempt(obj.getProp.bind(obj)); // 'abc'
         */
        this.attach('attempt', function attempt(fn) {
            try {
                var args = [].slice.call(arguments, 1),
                    res = fn.apply(null, args);
                return res === undefined ? true : res;
            } catch (e) {
                return false;
            }
        });

        /**
         * Re-arranges the arguments to a function so they match
         * the expected order of types. Useful for allowing developers
         * to specify arguments out of order (or optional arguments
         * before required arguments).
         *
         * Available type codes:
         *  - n: number
         *  - s: string
         *  - b: boolean
         *  - f: function
         *  - O: any object
         *  - a: array
         *  - d: date
         *  - r: regular expression
         *  - o: plain object
         *  - *: any defined value
         *
         * To mark a type as optional, simply follow it with ?:
         * `n?s` - an optional number but a required string
         *
         * See the example for details.
         * @function utils.misc#reorderByType
         * @param {Array|Arguments} arr The array of values to re-order.
         * @param {String} types The type codes to use to re-order the
         *  values array.
         * @returns {Array} The incoming values sorted according to
         *  the requested type order.
         * @example
         * function myFunc(strArg, numArg, dtArg) {
         *   // re-order the arguments into the expected order
         *   var args = utils.misc.reorderByType(arguments, 's?n?d');
         *   console.log(args.join('\n'));
         * }
         *
         * // myFunc(new Date());
         * // if the user only provided a date, it will
         * // be assigned to the 3rd array element, and
         * // `undefined` will be used for the first 2
         *
         * // myFunc('str', new Date());
         * // the 2nd array element will be set to
         * // `undefined`, but 'str' and date will
         * // be assigned to the other elements
         *
         * // myFunc(new Date(), 123);
         * // the 1st array element will be set to
         * // `undefined`, but 'num' and date will
         * // be assigned to the other elements in
         * // the correct order
         */
        this.attach('misc.reorderByType', function reorder(arr, types) {
            return reorderListByTypes(arr, types);
        });

        /**
         * Evaluates a list of arguments and returns
         * the first truthy value. You should pass
         * functions for your initial arguments and
         * a default object as your last argument. The
         * default value will be returned if all of the
         * other functions throw an exception or return
         * a falsy value.
         * @function utils#first
         * @param {Arguments} * A list of arguments to
         *  evaluate; the first truthy result is returned.
         * @returns {*}
         * @example
         * // get the first available storage device,
         * // or use cookies if no HTML5 storage exists
         * var storage = utils.first(
         *   localStorage,
         *   sessionStorage,
         *   document.cookies
         * );
         * @example
         * // get a preferred logger instance, or default
         * // to the console if none is available
         * var log = utils.first(
         *   // we use functions so any exceptions can be
         *   // safely caught and ignored
         *   function() { return log4js.get('myLog').info; },
         *   function() { return localStorage.get('logger').log; },
         *   function() { return grunt.log.writeln; },
         *   console.log // default
         * );
         */
        this.attach('first', function first() {

            var arg,
                args = [].slice.call(arguments),
                i = 0,
                l = args.length;

            for(; i < l; i++) {

                arg = args[i];

                if ('function' === typeof arg) {
                    arg = this.attempt(arg);
                }

                if (!!arg) {
                    return arg;
                }

            }

            return args.pop();

        });

    };

});
