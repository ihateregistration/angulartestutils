define(['utils', 'async'], function(utils, async) {

    'use strict';

    /**
     * @author Dan
     * @date 10/31/2014
     */

    /**
     * Contains methods related to data access.
     * @mixin data
     */

    var req = require;

    return function() {

        utils.apply(this);
        async.apply(this);

        /**
         * Loads the file at the specified require path and resolves
         * the returned promise with the file's return value.
         * @function data#require
         * @param {String} path The require path to load.
         * @param {Function} [success] An optional callback to invoke
         *  if require loads the file correctly.
         * @param {Function} [failure] An optional callback to invoke
         *  if require can *not* load the file.
         * @returns {Object} A synchronous promise that will be resolved
         *  or rejected based on the success of the RequireJS call.
         * @example
         * var utils = new TestUtils();
         * utils.data.require('path/to/json/object')
         *   .then(function(obj) {
         *     console.log(obj);
         *   })
         *   .catch(function(err) {
         *     console.error(err);
         *   });
         * @example
         * utils.data.require(
         *   '/path/to/json/object',
         *   function(obj) {
         *     console.log(obj);
         *   }
         * );
         */
        this.attach('data.require', function require(path, success, failure) {
            var defer = this.async.getSyncDefer();
            req([path], defer.resolve, defer.reject);
            return defer.promise.then(success, failure);
        });

    };

});
